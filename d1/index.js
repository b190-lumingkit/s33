// SECTION - JS Synchronous vs Asynchronous
// JavaScript is by default synchronous, meaning that only one statement will be executed at a time.
// This can be proven when a statement has an error, JavaScript will not proceed with the next statement
/* console.log("Hello!");
console.lgo("Hello Again!");
console.log("GoodBye!!"); */

// when statements take time to process, this slows down our code
// an example of this is when loops are used on a large amount of information, or when fetching data from databases
// for (let i = 1; i <= 1500; i++) {
//     console.log(i);
// }
// console.log("It's me again.");

// SECTION - ASYNCHRONOUS

// FETCH

// Fetch API - allows us to asynchronously request for a resource
// asynchronous - allows executing of codes simultaneously prioritizing on the less resource-consuming codes while the more complex and more resource-consuming codes run at the back
// A "Promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
/* 
    SYNTAX:
        fetch(URL);
*/
//console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// retrieves all posts following the REST API (retrieve, /posts, GET)
fetch("https://jsonplaceholder.typicode.com/posts")
// by using the .then method, we can now check the status of the promise
// fetch method will return a "promise" that resolves to a response
// the ".then" method captures the response object and returns another "promise" which will eventually be resolved or rejected
// .then(response => console.log(response));

// we use the "json" method from the response object to convert the data retrieved into JSON format to be used in our application
.then(response => response.json())
// using multiple then methods will result into promse chain
// then we can now access it and log in the console our desired output
.then(json => console.log(json))

console.log("Hello");
console.log("Hello Again!");
console.log("GoodBye!!");

// ASYNC - AWAIT
/* 
    "async" and "await" are other approach that can be used to perform asynchronous JavaScript
        - used in functions to indicate which portions of the code should be waited for
*/

// creates an asynchronous function
async function fetchData() {
    // waits for the fetch method to complete then stores it insde the "result" variable
    let result = await fetch("https://jsonplaceholder.typicode.com/posts");
    // result of returned fetch from the "result variable"
    console.log(result);
    // the returned value is an object
    console.log(typeof result);
    // we cannot access the content of the response object by directly accessing its body property
    console.log(result.body);

    // convert the data from the response object to JSON and store it inside the "json variable"
    let json = await result.json();
    // print the content of the response object
    console.log(json);
}

fetchData();

// SECTION - creating a post
/* 
    SYNTAX:
        fetch(URL, options)

        Example:
        fetch(URL, {
            method: "POST",
            headers: { "CONTENT-TYPE":"application/json" },
            body: JSON.stringify({
                title: "New Post",
                body: "Hello World",
                userId: 1
            })
        })
*/
// create a new post following REST IP (create, /posts, POST)
fetch("https://jsonplaceholder.typicode.com/posts", {
    // setting request method into "POST"
    method: "POST",
    // setting request headers to be sent to the backend
    // specified that the content headers will be sending JSON structure for its content 
    headers: {
        "CONTENT-TYPE":"application/json"
    },
    // set content/body data of the "request" object to be sent to the backend
    // JSON.stringify convertss the object into stringified JSON
    body: JSON.stringify({
        title: "New Post",
        body: "Hello World",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json));

// https://jsonplaceholder.typicode.com/posts/1

// SECTION - Updating a post
// updates a specific post following the REST API (/posts/:id, PUT)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    headers: {"CONTENT-TYPE":"application/json"},
    body: JSON.stringify({
        title: "Updated Post",
        body: "Hello Again",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json));


// SECTION - Updating a resource (PATCH)
// updates a specific post following the REST API (/posts/:id, PATCH)
/* 
    the difference between PUT and PATCH is the number of properties being updated

    PATCH is used to update a single property while maintaining the unupdated properties

    PUT is used when all of the properties need to be updated, or the whole document itself
*/
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PATCH",
    headers: {"CONTENT-TYPE":"application/json"},
    body: JSON.stringify({
        title: "Corrected Post",
    })
})
.then(response => response.json())
.then(json => console.log(json));


// SECTION - Deleting of a resource

fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "DELETE"
});


// SECTION - Filtering posts
// the data can be filtered by sending userId along with the URL
// information sent via url can be done by adding the question mark symbol (?)
/* 
    SYNTAX: 
        "<url>?parameterName=value" - single parameter
        "<url>?parameterNameA=valueA&&parameterNameB=valueB" - multiple parameter
*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(response => response.json())
.then(json => console.log(json));


// SECTION - Retrieving comments for a specific post/ accessing nested/embedded comments
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response => response.json())
.then(json => console.log(json));