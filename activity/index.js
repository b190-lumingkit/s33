// GET - retrieve all to do list items and print onlt the title of every item
async function getAllTodo() {
    let dataObj = await fetch(`https://jsonplaceholder.typicode.com/todos`);
    let dataJson = await dataObj.json();

    console.dir(dataJson.map(data => data.title));

    // synchronous running of async functions for output purposes
    await getTodo();
    await addTodo();
    await updateTodo(1);
    await changeTodo(2);
    await patchTodo(3);
    await completeThisTodo(4);
    await deleteTodo(5);
}
getAllTodo();

// GET - retrieve a single to do list and print the title and status of the to do list
async function getTodo(id=1) {
    let todoObj = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`);
    let todoJson = await todoObj.json();

    console.log(`The item "${todoJson.title}" has a status of "completed: ${todoJson.completed}"`)
}

// POST - create a to do list item
async function addTodo() {
    let newTodo = await fetch(`https://jsonplaceholder.typicode.com/todos`, 
    {
        method: "POST",
        headers: {"CONTENT-TYPE":"application/json"},
        body: JSON.stringify({
            title: "[New] A new To Do list item",
            completed: false,
            userId: 2
        })
    })
    
    newTodo = await newTodo.json();
    console.dir(newTodo);
}

// PUT - update a to do list item
async function updateTodo(id) {
    let updateTodo = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, 
    {
        method: "PUT",
        headers: {"CONTENT-TYPE": "application/json"},
        body: JSON.stringify({
            title: `[Updated-PUT] To Do List item with id: ${id}`,
            completed: false,
            userId: 2
        })
    })

    updateTodo = await updateTodo.json();
    console.dir(updateTodo);
}

// PUT - update a to do list item by changing the data structure
async function changeTodo(id) {
    let changeTodo = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`,
    {
        method: "PUT",
        headers: {"CONTENT-TYPE": "application/json"},
        body: JSON.stringify({
            title: `[Updated-Change Data Structure] To Do List with id: ${id}`,
            description: `To update a to do list item with a different data structure`,
            status: `Pending`,
            dateCompleted: `Pending`,
            userId: 2
        })
    })

    changeTodo = await changeTodo.json();
    console.dir(changeTodo);
}

// PATCH - update a to do list item with PATCH
async function patchTodo(id) {
    let changeTodo = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`,
    {
        method: "PATCH",
        headers: {"CONTENT-TYPE": "application/json"},
        body: JSON.stringify({
            title: `[PATCHED] Updated To Do list item with id: ${id}`,
        })
    })

    changeTodo = await changeTodo.json();
    console.dir(changeTodo);
}

// PATCH - update a to do list by changing the status to complete and add a completion data
async function completeThisTodo(id) {
    // add [COMPLETED] at the beginning of todo.title
    let data = await fetch (`https://jsonplaceholder.typicode.com/todos/${id}`);
    data = await data.json();

    let completeThisTodo = await fetch (`https://jsonplaceholder.typicode.com/todos/${id}`,
    {
        method: "PATCH",
        headers: {"CONTENT-TYPE":"application/json"},
        body: JSON.stringify({
            title: `[COMPLETED] ${data.title}`,
            completed: true,
            dateCompleted: `7/12/22`
        })
    })

    completeThisTodo = await completeThisTodo.json();
    console.dir(completeThisTodo);
}

async function deleteTodo(id) {
    let data = await fetch (`https://jsonplaceholder.typicode.com/todos/${id}`);
    data = await data.json();

    await fetch (`https://jsonplaceholder.typicode.com/todos/${id}`, 
    {
        method: "DELETE"
    });

    console.log(`This to do item was deleted "Title: ${data.title}"`);
}

